<?php
    require "_users.php";
    require "_recipes.php";
    session_start();
    $theme = isset($_SESSION['theme']) ? $_SESSION['theme'] : "light";//theme
    if (isset($_POST['add_recipe'])) {
        $uid = isset($_SESSION['uid']) ? $_SESSION['uid'] : NULL;
        if ($uid) { //pokud jsme přihlášeni, vše co bylo posláno uložíme
            $user = getUserByUid($uid);
            $username=$user['username'];
            $name=$_POST['name'];
            $complexity=$_POST['complexity'];
            $type=$_POST['type'];
            $cooking_time=$_POST['cooking_time'];
            //ukládání obrázku
            $strtotime = strtotime("now");
            $imagename = $strtotime.'_'.$_FILES['img']['name'];
            $imagetype = $_FILES['img']['type'];
            $imageerror = $_FILES['img']['error'];
            $imagetemp = $_FILES['img']['tmp_name'];
            $imagePath = "recipe_images/"; //složka, do které ho chci dát
            $recipe_description_area=$_POST['recipe_description_area'];
            $recipe_text_area=$_POST['recipe_text_area'];
            if(is_uploaded_file($imagetemp)) {
                if(move_uploaded_file($imagetemp, $imagePath . $imagename)) {//pokud se obrázek podařilo nahrát
                    addNewRecipe($username, $name, $complexity, $type, $cooking_time, $recipe_description_area, $recipe_text_area, $imagename, $strtotime);
                    header('Location: recipe_added.php');
                }
                else {
                    echo "Failed to move your image.";
                }
            }
            else {//pokud se obrázek nepodařilo nahrát
                $imagename= "deafult.png"; //nahrajeme deafutní obrázek
                addNewRecipe($username, $name, $complexity, $type, $cooking_time, $recipe_description_area, $recipe_text_area, $imagename, $strtotime);
                header('Location: recipe_added.php');
            }
        } else {
            header('Location: added_without_reg.php');
        }        
    }
   
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="http://localhost/zwo-sem-prace/css/index.css">
    <title>Přidat recept</title>
    <meta charset="utf-8" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <script src="validate.js"></script> <!--script na validaci formuláře-->
</head>
<body class="<?php echo($theme)?>">
<div class="container">
<!--Menu-->
  <div class="box">
    <h1 class="menu">Menu</h1>
    <ul>
      <li><a href="hlavni_stranka.php">Hlavní stránka</a></li>
      <li><a href="index.php">Nejstarší</a></li>
      <li><a href="polevky.php">Polévky</a></li>
      <li><a href="hlavni_chody.php">Hlavní chody</a></li>
      <li><a href="dezerty.php">Dezerty</a></li>
      <?= isset($uid)? '<li><a href="logout.php">Odhlásit se</a></li>' : ''?>
    </ul>
  </div>

  <div class="background">
    <!--Přidání receptu-->
    <h1 class="title">Přidej recept</h1>
    <form action="" method="post" name="addForm" enctype="multipart/form-data">
        <div class="recipe_creation">
            <label for="jmeno">Nadpis</label>
            <input type="text" name="name" placeholder="Povinny udaj, minimalne 2 znaky" id="jmeno" required>   
        </div>

        <div class="recipe_creation">
            <label for="complexity">Slozitost</label>
            <select name="complexity">
                <option>lehká</option>
                <option>střední</option>
                <option>težká</option>
              </select>
        </div>

        <div class="recipe_creation">
            <label for="type">Typ</label>
            <select name="type">
                <option></option>
                <option>Polevky</option>
                <option>Hlavni chody</option>
                <option>Dezerty</option>
              </select>
        </div>

        <div class="add_image_button">
            <label for="img">Obrázek</label>
            <input name="img" type="file" >
        </div>

        <div class="recipe_creation">
            <label for="cooking_time">Doba</label>
            <input type="time" name="cooking_time" >   
        </div>

        <div class="recipe_creation">
            <label for="description">Popis</label>
            <textarea class= "recipe_description" name="recipe_description_area" id="description" required>
            </textarea>
        </div>

        <div class="recipe_creation">
            <label for="text_area">Recept</label>
            <textarea class= "recipe_text_area" name="recipe_text_area" id="recipe_text" required>
            </textarea>
        </div>
        <input type="submit" value="Potvrdit" name="add_recipe" >  
        </form>
        <script>
        init();
    </script>
    </div>
</div>
</body>
</html>