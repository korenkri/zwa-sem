<?php
    require "_users.php";
    session_start();
    $uid = isset($_SESSION['uid']) ? $_SESSION['uid'] : NULL; 
    $theme = isset($_SESSION['theme']) ? $_SESSION['theme'] : "light"; //theme
    if (isset($_POST['button1'])) { //nastavení theme
      $_SESSION['theme']=$_POST['button1'];
      $theme=$_POST['button1'];
    }
    elseif(isset($_POST['button2'])) {//nastavení theme
      $_SESSION['theme']=$_POST['button2'];
      $theme=$_POST['button2'];
    }
    
    if ($uid) { //nastavení uid
        $user = getUserByUid($uid);
    }
  
?>

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="http://localhost/zwo-sem-prace/css/index.css">
    <title>Vítejte v on-line kuchařce!</title>
    <meta charset="utf-8" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" /> 
</head>

<body class="<?php echo($theme)?>">
<div class="container">
  <!-- Menu -->
  <div class="box">
    <h1 class="menu">Menu</h1>
    <ul>
        <li><a href="hlavni_stranka.php">Hlavní stránka</a></li>
      <li><a href="index.php">Nejstarší</a></li>
      <li><a href="polevky.php">Polévky</a></li>
      <li><a href="hlavni_chody.php">Hlavní chody</a></li>
      <li><a href="dezerty.php">Dezerty</a></li>
      <?= isset($uid)? '<li><a href="recipe_add.php">Přidat recept</a></li>' : ''?>
      <?= isset($uid)? '<li><a href="logout.php">Odhlásit se</a></li>' : ''?>
      <?= isset($uid)? '' : '<li><a href="login.php">Přihlásit se</a></li>'?>
      <?= isset($uid)? '' : '<li><a href="register.php">Registrace</a></li>'?>
    
    </ul>
  </div>
<!-- Text stránky -->
  <div class="background">
    <h1 class="title">Vítejte v on-line kuchařce!
      <form method="post">
      <?= $theme=='dark' ? '<button type="submit" name="button2" class="button_theme" value="light"> <img class="icon" alt="icon" src="images/cookbook_light.png"/></button>' 
      : '<button type="submit" name="button1" class="button_theme" value="dark"> <img class="icon" alt="icon" src="images/cookbook_dark.png"/></button>' ?> 
      </form>
      <div class="welcome_img"><img alt="knizka" src="images/book.png"/></div>  
    </h1>
  </div>  
</div>
<!-- Footer s autorem -->
<footer class="author">
  <p>Author: Kristýna Kořenská</p>
</footer>
</body>
</html>