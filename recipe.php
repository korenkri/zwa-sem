<?php
    $currentRecipe=$_GET['recipeId'];
    session_start();
    $theme = isset($_SESSION['theme']) ? $_SESSION['theme'] : "light"; //theme
    if (isset($_POST['button1'])) { //nastavení theme
      $_SESSION['theme']=$_POST['button1'];
      $theme=$_POST['button1'];
      
    }
    elseif(isset($_POST['button2'])) { //nastavení theme
      $_SESSION['theme']=$_POST['button2'];
      $theme=$_POST['button2'];
     
    }
?>

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="http://localhost/zwo-sem-prace/css/index.css">
    <title>Kuchařka</title>
    <meta charset="utf-8" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
</head>
<body class="<?php echo($theme)?>">
<div class="container">
  <!--Menu-->
  <div class="box">
    <h1 class="menu">Menu</h1>
    <ul>
      <li><a href="hlavni_stranka.php">Hlavní stránka</a></li>
      <li><a href="index.php">Nejstarší</a></li>
      <li><a href="polevky.php">Polévky</a></li>
      <li><a href="hlavni_chody.php">Hlavní chody</a></li>
      <li><a href="dezerty.php">Dezerty</a></li>
      <?= isset($uid)? '<li><a href="recipe_add.php">Přidat recept</a></li>' : ''?>
      <?= isset($uid)? '<li><a href="logout.php">Odhlásit se</a></li>' : ''?>
      <?= isset($uid)? '' : '<li><a href="login.php">Přihlásit se</a></li>'?>
      <?= isset($uid)? '' : '<li><a href="register.php">Registrace</a></li>'?>
    </ul>
  </div>
  <!--Zobrazení receptu-->
  <?php   
    $file = 'recipes.json';
    $recipes = json_decode(file_get_contents($file), true);//otevírání a čtení ze složky
      foreach($recipes as $recipe):
        if($currentRecipe==$recipe['id']){
          $recipename=$recipe["recipe_name"]; 
          $recipetext=$recipe["recipe_text_area"];  
          $recipeimage=$recipe["imagename"];
          $recipetime=$recipe["cooking_time"];
          $recipecomplexity=$recipe["complexity"];
          break;
        }
      
    ?>
<?php endforeach ?>
<!--Samotný recept-zobrazení-->
  <div class="background">
    <h1 class="title"><form method="post">
      <?= $theme=='dark' ? '<button type="submit" name="button2" class="button_theme" value="light"> <img class="icon" alt="icon" src="images/cookbook_light.png"/></button>' 
      : '<button type="submit" name="button1" class="button_theme" value="dark"> <img class="icon" alt="icon" src="images/cookbook_dark.png"/></button>' ?> 
      </form></h1>
    <div>
        <h1 class="title"><?php echo htmlspecialchars($recipename)?></h1>
    </div>
    <div class="recipe_img"><img class="img" alt="babovka" src="recipe_images/<?php echo $recipeimage?>"/></div>
    <div class="recipe_info">
    <h2>Obtížnost</h2>
        <p>
        <?php if ($recipecomplexity !="") { echo $recipecomplexity;} //pokud je uvedena obtížnost, zobrazím ji, pokud ne, vypíšu neuvedeno
             else { echo ("Neuvedeno");}
            ?>
        </p>
    </div>
    <div class="recipe_info">
    <h2>Čas přípravy a vaření</h2>
        <p>
        <?php if ($recipetime !="") { echo $recipetime;} //to samé, jen pro čas
             else { echo ("Neuvedeno");}
            ?>
        </p>
    </div>
    <div class="recipe_instructions">
        <h2>Postup přípravy</h2>
        <p>
        <?php echo htmlspecialchars($recipetext)?> 
        </p>
    </div>
  </div>
</div>
</body>
</html>