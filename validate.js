(function(global) {
    let name = null;
    let description=null;
    let formEl = null;
    let recipe_text=null

    function init() {
       name = document.querySelector('#jmeno');
       description=document.querySelector('#description');
       recipe_text=document.querySelector('#recipe_text');
       formEl = document.querySelector('form');
       formEl.addEventListener('submit', validate);
    }

    function validate(e) {
        const nameResult = validateName();
        const descriptionResult= validateDescription();
        const textResult= validateRecipe();
        console.log(descriptionResult, textResult, nameResult)
        if (!nameResult || !descriptionResult || !textResult) {
            e.preventDefault();
        }
    }

    function validateName() {

        if (name.value.length > 2) {
            name.parentElement.classList.remove('error');
            return true;
        } else {
            name.parentElement.classList.add('error');
            return false;
        }
    }

    function validateDescription() {

        if (description.value.length > 10) {
            description.parentElement.classList.remove('error');
            return true;
        } else {
            description.parentElement.classList.add('error');
            return false;
        }
    }

    function validateRecipe() {

        if (recipe_text.value.length > 30) {
            recipe_text.parentElement.classList.remove('error');
            return true;
        } else {
            recipe_text.parentElement.classList.add('error');
            return false;
        }
    }

    global.init = init;
})(window)