<?php
    session_start();
    $theme = isset($_SESSION['theme']) ? $_SESSION['theme'] : "light";
    if (isset($_POST['button1'])) {
      $_SESSION['theme']=$_POST['button1'];
      $theme=$_POST['button1'];
      
    }
    elseif(isset($_POST['button2'])) {
      $_SESSION['theme']=$_POST['button2'];
      $theme=$_POST['button2'];
     
    }
?>

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="http://localhost/zwo-sem-prace/css/index.css">
    <title>Vítej uživateli</title>
    <meta charset="utf-8" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
</head>
<body class="<?php echo($theme)?>">
<div class="container">
  <div class="box">
    <h1 class="menu">Menu</h1>
    <ul>
      <li><a href="hlavni_stranka.php">Hlavní stránka</a></li>
      <li><a href="index.php">Nejstarší</a></li>
      <li><a href="polevky.php">Polévky</a></li>
      <li><a href="hlavni_chody.php">Hlavní chody</a></li>
      <li><a href="dezerty.php">Dezerty</a></li>
      <li><a href="login.php">Přihlásit se</a></li>
    </ul>
  </div>

  <div class="background">
    <h1 class="title"><img class="icon" alt="icon" src="images/user_icon.png"/></h1>
    <form action="" method="post" >
        <fieldset>
            <legend>Vítej</legend>
            <h1 class="title">Vítej nový uživateli!</h1>
            <p>Pokračuj prosím tím, že se přihlásíš. Jakožto přihlášený budeš mít možnost nejen recepty číst ale dokonce můžeš přidávat i své vlastní. Pro přihlášení klikni na tlačítko přihlásit, které se nachází v menu. </p> 
        </fieldset>
        <?php
            if (isset($error)) {
                echo "<p>$error</p>";
            }
        ?>

      </form>
  </div>
    

</div>

</body>
</html>