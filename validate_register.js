(function(global) {
    let name = null;
    let password=null;
    let form = null;
    

    function init() {
        name=document.querySelector('#name');
       password=document.querySelector('#password');
       form = document.querySelector('form');
       form.addEventListener('submit', validate);
    }

    function validate(e) {
        const nameResult = validateName();
        const passwordResult= validatePassword();
        if (!nameResult || !passwordResult) {
            e.preventDefault();
        }
    }

    function validateName() {

        if (name.value.length > 2) {
            name.parentElement.classList.remove('error');
            return true;
        } else {
            name.parentElement.classList.add('error');
            return false;
        }
    }

    function validatePassword() {

        if (password.value.length > 6) {
            password.parentElement.classList.remove('error');
            return true;
        } else {
            password.parentElement.classList.add('error');
            return false;
        }
    }

    global.init = init;
})(window)