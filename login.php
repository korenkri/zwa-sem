<?php
//todo validace pomoci js
    require '_users.php';
    session_start();
    $theme = isset($_SESSION['theme']) ? $_SESSION['theme'] : "light"; //theme
    $error ='';
    if (isset($_POST['login'])) { //když kliknu na potvrdit
      $username = $_POST['username'];//poslané už.jméno
      $password = $_POST['password'];//poslané už.heslo
        if ($username && $password) {//když mám obojí
            $user = getUserByUserName($username);
            if ($user) {
                if (password_verify($password,$user['password'])) { //ověřování hesla
                    $_SESSION['uid'] = $user['id'];
                    header('Location: hlavni_stranka.php');//přesměrování na hlavní stránku
                } else {
                  $error = 'Nespravne udaje';  //špatně vyplňeno
                }
            } else {
                $error = 'Nespravne udaje';
            }
        } else { //když nemám obojí
            $error = 'Nespravne udaje';
        }
    }
?>

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="http://localhost/zwo-sem-prace/css/index.css">
    <title>Prihlasovani</title>
    <meta charset="utf-8" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <script src="validate_register.js"></script>
</head>
<body class="<?php echo($theme)?>">

<div class="container">
  <!--Menu-->
  <div class="box">
    <h1 class="menu">Menu</h1>
    <ul>
      <li><a href="hlavni_stranka.php">Hlavní stránka</a></li>
      <li><a href="index.php">Nejstarší</a></li>
      <li><a href="polevky.php">Polévky</a></li>
      <li><a href="index.php">Hlavní chody</a></li>
      <li><a href="dezerty.php">Dezerty</a></li>
    </ul>
  </div>

  <div class="background">
    <h1 class="title"></h1>
    <!--Formulář-->
    <form action="" method="post" > <!--enctype="multipart/form-data"-->
        <fieldset>
            <legend>Přihlášení</legend>
            <h1 class="title">Přihlášení</h1>
            <div class="user_name">
                <label for="name" >Uživatelské jméno</label>
                <input type="text" name="username" id="name" placeholder="<?php echo $error ?>" required>   
            </div>
            <div class="user_password">
                <label for="pwd">Heslo</label>
                <input type="password" name="password" id="password" placeholder="<?php echo $error ?>" required>   
            </div>
            <input type="submit" value="Potvrdit" name="login" >    
        </fieldset>
      </form>
      <script>
        init();
    </script>
  </div>
</div>

</body>
</html>