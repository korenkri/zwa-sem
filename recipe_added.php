<?php
    require "_users.php";
    session_start();
    $theme = isset($_SESSION['theme']) ? $_SESSION['theme'] : "light";//theme
    if (isset($_POST['button1'])) {
      $_SESSION['theme']=$_POST['button1'];
      $theme=$_POST['button1']; 
    }
    elseif(isset($_POST['button2'])) {
      $_SESSION['theme']=$_POST['button2'];
      $theme=$_POST['button2'];
    }
    
    $uid = isset($_SESSION['uid']) ? $_SESSION['uid'] : NULL;
    if ($uid) {
        $user = getUserByUid($uid);
    } else {
      header('Location: hlavni_stranka.php');
    }
?>

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="http://localhost/zwo-sem-prace/css/index.css">
    <title>Kuchařka</title>
    <meta charset="utf-8" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
</head>
<body class="<?php echo($theme)?>">
<div class="container">
  <!--Menu-->
  <div class="box">
    <h1 class="menu">Menu</h1>
    <ul>
      <li><a href="hlavni_stranka.php">Hlavní stránka</a></li>
      <li><a href="index.php">Nejstarší</a></li>
      <li><a href="polevky.php">Polévky</a></li>
      <li><a href="hlavni_chody.php">Hlavní chody</a></li>
      <li><a href="dezerty.php">Dezerty</a></li>
      <?= isset($uid)? '<li><a href="recipe_add.php">Přidat recept</a></li>' : ''?>
      <?= isset($uid)? '<li><a href="logout.php">Odhlásit se</a></li>' : ''?>
      <?= isset($uid)? '' : '<li><a href="login.php">Přihlásit se</a></li>'?>
      <?= isset($uid)? '' : '<li><a href="register.php">Registrace</a></li>'?>
    </ul>
  </div>

  <div class="background">
    <h1 class="title">Výborně, přidal jsi nový recept!</h1>
    <?= $theme=='light' ? '<img class="added_image" src="images/added_dark.gif" alt="Add recipe" />' : '<img class="added_image" src="images/added.gif" alt="Add recipe" />' ?><!--Změna gifu podle theme-->    
  </div>
</div>
</body>
</html>