<?php
    // zrušíme session a přesměrujeme
    session_start();
    session_unset();
    session_destroy();
    header('Location: hlavni_stranka.php');
?>