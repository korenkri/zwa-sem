<?php
    // funkce na získávání receptu podle unikátního id
    function getRceipeByUid($recipes, $uid) {
        foreach ($recipes as $recipe) {
            if($uid == $recipe['id']) {
                return $recipe;
            }
        }

        return NULL;
    }
    // funkce na získávání všech receptů určitého typu, pokud typ není definován(Nejstarší), vracím všechny
    function getAllRecipes($type="all", $page, $items_on_page) {
        $file = 'recipes.json';
        $recipes = json_decode(file_get_contents($file), true);
        $selected_recipes = array();
        $skiped_items = 0;

        foreach ($recipes as $recipe) {
            if($skiped_items >= ($page)*$items_on_page){
                break;
            }
            if($skiped_items >= ($page - 1)*$items_on_page){
                if($type=='all' || $type == $recipe['type']){
                    array_push($selected_recipes, $recipe);
                }
            }
            if($type=='all' || $type == $recipe['type']){
                $skiped_items += 1;
            }
            
        }
        return $selected_recipes;
    }
    // funkce, co vrací počet stránek pro určitý typ
    function getNumberOfPages($items_on_page, $type="all") {
        $file = 'recipes.json';
        $recipes = json_decode(file_get_contents($file), true);
        $number_of_recipes=0;
        foreach ($recipes as $recipe) {
            if($type=='all' || $type == $recipe['type']){
                $number_of_recipes += 1;
            } 
        }
        return ceil($number_of_recipes / $items_on_page);
    }
    // funkce na přidávání receptu
    function addNewRecipe($username, $name, $complexity, $type, $cooking_time, $recipe_description_area, $recipe_text_area, $imagename, $strtotime) {
        $file = 'recipes.json';
        $recipes = json_decode(file_get_contents($file), true);
		$recipe = array(
			"recipe_name" => $name,
			"id" => uniqid(),
            "complexity"=>$complexity,
            "type"=>$type,
            "cooking_time"=>$cooking_time,
            "recipe_description_area"=>$recipe_description_area,
            "recipe_text_area"=>$recipe_text_area,
            "imagename"=>$imagename,
            "user"=>$username,
            "time"=> $strtotime
		);
		array_push($recipes, $recipe);
        file_put_contents($file,json_encode($recipes));
		return;
	}
?>