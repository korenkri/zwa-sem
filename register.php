<?php
    require '_users.php';
    $exists=false;
    $username="";
    $birth ="";
    $exp="";
    session_start();
    $theme = isset($_SESSION['theme']) ? $_SESSION['theme'] : "light";//theme
    if (isset($_POST['register'])) {//registrace
      $file = 'users.json';
      $users = json_decode(file_get_contents($file), true);
      $username = (isset($_POST['username'])) ? $_POST['username'] : '';
      $password = $_POST['password'];
      $birth = (isset($_POST['birth'])) ? $_POST['birth'] : '';
      $exp = (isset($_POST['experience'])) ? $_POST['experience'] : '';
      $exists = false;
      foreach($users as $user){
        if($username==$user['username']){ //pokud existuje již takový user, musí zadat jiné jméno
          $exists=true;
          break;
        }
      }
        if ($username && $password && !$exists) { //pokud uživatelké jméno ještě neexistuje a je nastaveno správě jméno i heslo(povinné údaje)
            $user = addNewUser($username, $password, $birth, $exp ); //přidávám nového uživatele do databáze
            $username="";  
            $_SESSION['uid'] = $user['id'];
            header('Location: welcome.php');  
        }
    }
?>

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="http://localhost/zwo-sem-prace/css/index.css">
    <title>Kuchařka</title>
    <meta charset="utf-8" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <script src="validate_register.js"></script>
</head>
<body class="<?php echo($theme)?>">
<div class="container">
  <!--Menu-->
  <div class="box">
    <h1 class="menu">Menu</h1>
    <ul>
      <li><a href="index.php">Nejstarší</a></li>
      <li><a href="polevky.php">Polévky</a></li>
      <li><a href="hlavni_chody.php">Hlavní chody</a></li>
      <li><a href="dezerty.php">Dezerty</a></li>
    </ul>
  </div>

  <div class="background">
    <h1 class="title"><img class="icon" alt="icon" src="images/user_icon.png"/></h1>
    <!--Formulář-->
    <form action="" method="post" > <!--enctype="multipart/form-data"-->
        <fieldset>
            <legend>Registrace</legend>
            <h1 class="title">Registrace</h1>
            <div class="new_user_name">
                <label for="name">Uživatelské jméno</label>
                <input type="text" name="username" id="name" value="<?php echo $username ?>" required>
            </div>

            <div class="new_user_password" required>
                <label for="pwd">Nové heslo</label>
                <input type="password" name="password" placeholder="Zadej heslo, minimálně 6 znaků" id="password">   
            </div>

            <div class="new_user_birth">
                <label for="birth">Datum narození</label>
                <input type="date" name="birth" value=<?php echo $birth ?>>   
            </div>

            <div class="new_user_experience">
                <label for="experience" >Zkušenost s vařením</label>
                <select name="experience">
                  <option value="novacek" <?php if ("novacek"==$exp) echo "selected=selected"; ?>>novacek</option> <!--Upravit-->
                  <option value="amater" <?php if ("amater"==$exp) echo "selected=selected"; ?>>amater</option>
                  <option value="sef" <?php if ("sef"==$exp) echo "selected=selected"; ?>>sef</option>
                </select>
            </div>
            <input type="submit" value="Potvrdit" name="register" >  
        </fieldset>
        </form>
        <script>
        init();
    </script>
  </div>
</div>
</body>
</html>