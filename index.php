<?php
    require "_users.php";
    require "_recipes.php";
    session_start();
    $page;
    if(!isset($page)){
      $page = 1;
    }

    $items_on_page = 3; //počet receptů na stránce
    $max_page = getNumberOfPages($items_on_page); //počet stránek
    if(isset($_GET['page']) && 1 >= $_GET['page']){
      $page = 1;
    }
    elseif(isset($_GET['page']) && $max_page <= $_GET['page']){
      $page = $max_page;
    }
    elseif (isset($_GET['page'])) {
      $page = $_GET['page'];
    }

    $uid = isset($_SESSION['uid']) ? $_SESSION['uid'] : NULL;
    $theme = isset($_SESSION['theme']) ? $_SESSION['theme'] : "light"; //theme
    if (isset($_POST['button1'])) { //nastavení theme
      $_SESSION['theme']=$_POST['button1'];
      $theme=$_POST['button1'];
      
    }

    elseif(isset($_POST['button2'])) { //nastavení theme
      $_SESSION['theme']=$_POST['button2'];
      $theme=$_POST['button2'];
    }

    if ($uid) { //nastavení uid
        $user = getUserByUid($uid);
    }

?>

<!DOCTYPE html>
<html>
<head>
    
    <link rel="stylesheet" href="http://localhost/zwo-sem-prace/css/index.css">
    <title>Kuchařka</title>
    <meta charset="utf-8" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    
</head>
<body class="<?php echo($theme)?>">
<div class="container">
  <!-- Menu -->
  <div class="box">
    <h1 class="menu">Menu</h1>
    <ul>
    <li><a href="hlavni_stranka.php">Hlavní stránka</a></li>
      <li><a href="index.php">Nejstarší</a></li>
      <li><a href="polevky.php">Polévky</a></li>
      <li><a href="hlavni_chody.php">Hlavní chody</a></li>
      <li><a href="dezerty.php">Dezerty</a></li>
      <?= isset($uid)? '<li><a href="recipe_add.php">Přidat recept</a></li>' : ''?>
      <?= isset($uid)? '<li><a href="logout.php">Odhlásit se</a></li>' : ''?>
      <?= isset($uid)? '' : '<li><a href="login.php">Přihlásit se</a></li>'?>
      <?= isset($uid)? '' : '<li><a href="register.php">Registrace</a></li>'?>
    
    </ul>
  </div>
<!-- Recepty na stránce -->
  <div class="background">
    <h1 class="title">Nejstarší
      <form method="post">
      <?= $theme=='dark' ? '<button type="submit" name="button2" class="button_theme" value="light"> <img class="icon" alt="icon" src="images/cookbook_light.png"/></button>' 
      : '<button type="submit" name="button1" class="button_theme" value="dark"> <img class="icon" alt="icon" src="images/cookbook_dark.png"/></button>' ?> 
      </form>  
    </h1>

    <?php   
    $recipes = getAllRecipes("all", $page, $items_on_page);
    foreach($recipes as $recipe):
      $recipename=$recipe["recipe_name"]; 
      $recipedescription=$recipe["recipe_description_area"];  
      $recipeimage=$recipe["imagename"];
      $recipeid=$recipe['id'];
    ?>
    <div class="recipe">
      <div class="recipe_image"><img alt="recept1" src="recipe_images/<?php echo $recipeimage?>"/></div>
      <div class="recipe_text">
        <h1 class="recipe_name"><?php echo htmlspecialchars($recipename)?></h1>
        <p class="description">
        <?php echo htmlspecialchars($recipedescription)?>
        </p>
      </div>
      <div class="recipe_button">
        <a href="http://localhost/zwo-sem-prace/recipe.php?recipeId=<?php echo $recipeid?>" >Více</a>
      </div>
    </div>
    <?php endforeach ?>
    <!-- Tlačítka další a předchozí -->
    <div class="buttons">
      <div class="previous"> 
        <a class="button_previous in" href="index.php?page=<?php echo $page - 1 ?>">Předchozí</a>
      </div>
      <div class="previous"> 
        <?php echo $page ?>/<?php echo $max_page ?>
      </div>
      <div class="next"> 
        <a  class="button_next in"  href="index.php?page=<?php echo $page + 1 ?>">Dalsi</a>
      </div>
    </div>
  </div>  
</div>

<footer class="author">
  <p>Author: Kristýna Kořenská</p>
</footer>
</body>
</html>