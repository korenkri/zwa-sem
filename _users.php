<?php
    //tato funkce umožní vyhledat uživatele podle jména
    function getUserByUserName($username) {
        $file = 'users.json';
        $users = json_decode(file_get_contents($file), true);
        foreach ($users as $user) {
            if($username == $user['username']) {
                return $user;
            }
        }
        return NULL;
    }
    //tato funkce umožní vyhledat uživatele podle id
    function getUserByUid($uid) {
        $file = 'users.json';
        $users = json_decode(file_get_contents($file), true);
        foreach ($users as $user) {
            if($uid == $user['id']) {
                return $user;
            }
        }

        return NULL;
    }
    //Přidání nového uživatele
    function addNewUser($username, $password, $birth, $exp) {
        $file = 'users.json';
        $users = json_decode(file_get_contents($file), true);
		$user = array(
			"username" => $username,
			"password" => password_hash($password, PASSWORD_DEFAULT),
			"id" => uniqid(),
            "birth"=>$birth,
            "experince"=>$exp
		);
		array_push($users, $user);
        file_put_contents($file,json_encode($users));
		return;
	}
?>